# TourGuide application 

TourGuide est une application d’aide au voyage qui a pour public cible les touristes, cette
application suit les utilisateurs par géolocalisation afin de leur faire des propositions
d’attractions touristiques à proximité.
Les utilisateurs de l’application gagnent des points de récompense à chaque attraction
touristique visitée.
Ces points de récompenses permettent de bénéficier de réductions auprès d’agences de
voyage associées.
Des propositions de voyages sont présentées à chaque utilisateur dans l’application en
fonction de ses préférences.

## Configuration Application en Microservices

#### TourGuide (application principal) :

- Java 11
- Spring Boot 2.7.7
- Gradle 8

#### GpsUtil :

- Java 11
- Spring Boot 2.7.7
- Gradle 8

#### Rewards :

- Java 11
- Spring Boot 2.7.7
- Gradle 8

#### TripPricer :

- Java 11
- Spring Boot 2.7.7
- Gradle 8

## Documentation Technique (Swagger)

#### TourGuide :

http://localhost:8000/swagger-ui.html#/

#### GpsUtil :

http://localhost:8001/swagger-ui.html#/

#### Rewards :

http://localhost:8002/swagger-ui.html#/

#### TripPricer : 

http://localhost:8003/swagger-ui.html#/

## Conception de schémas techniques

![shéma technique 1](/source/images/Shema1.png)

![shéma technique 2](/source/images/shema.png)